# API de Reconocimiento de Entidades Nombradas (NER) con spaCy en Flask

## Descripción
Esta es una API REST sencilla desarrollada en Python utilizando el microframework Flask. La API realiza el reconocimiento de entidades nombradas (NER) en oraciones en español utilizando la biblioteca spaCy con el modelo "es_core_news_sm".

## Instalación
1. Clonar este repositorio.

2. Instalar las dependencias necesarias utilizando pip3 y el archivo requirements.txt. Ejecutando el siguiente comando en la terminal:
```
pip3 install -r requirements.txt
```
Esto instalará Flask y spaCy en un entorno de Python3.

## Uso
1. Ejecutar la API utilizando el siguiente comando en la terminal:
```
python3 app.py
```
Esto iniciará el servidor de desarrollo en `http://127.0.0.1:4000/`.

2. Ejecutar los unit tests utilizando el siguiente comando en la terminal:
```
python3 tests.py
```
Esto ejecutará los 12 tests diseñados para probar la API.

3. También se puede probar mediante solicitudes POST independientes con herramientas como Postman.
Para esto es necesario hacer la solicitud POST a la siguiente URL con un JSON que contenga una lista de oraciones en español:

http://localhost:4000/ner

Ejemplo de JSON:

{
"oraciones": [
"Apple está buscando comprar una startup del Reino Unido por mil millones de dólares.",
"San Francisco considera prohibir los robots de entrega en la acera."
]
}

Y regresará:

{
"resultado": [
                {
                    "entidades": {
                        "Apple": "ORG",
                        "Reino Unido": "LOC"
                    },
                    "oración": "Apple está buscando comprar una startup del Reino Unido por mil millones de dólares."
                },
                {
                    "entidades": {
                        "San Francisco": "LOC"
                    },
                    "oración": "San Francisco considera prohibir los robots de entrega en la acera."
                }
            ]
}

Tal y como se pide en las instrucciones.


4. La API devolverá un JSON que contiene una lista de las entidades identificadas en cada oración, junto con el tipo de cada entidad.

## Tests
Se han realizado varios unittests para evaluar la ejecución de la API en diferentes escenarios. Los tests incluyen:

- Pruebas con oraciones válidas en español.
- Pruebas con listas de oraciones vacías.
- Pruebas con oraciones inválidas que no contienen entidades nombradas.
- Pruebas con un JSON inválido y una solicitud POST vacía.
- Pruebas con muchas oraciones.
- Pruebas con oraciones con muchas entidades.
- Pruebas con key invalida en vez de oraciones.
- Pruebas con caracteres especiales como signos de interrogación y exclamación.
- Pruebas con oraciones sin entidades.

Se pueden agregar más pruebas "unit tests" en caso de ser necesario para evaluar el funcionamiento de la API.

## Detalles técnicos
- La API está desarrollada en Python con Flask como microframework.
- Se utiliza la biblioteca spaCy con el modelo "es_core_news_sm" para realizar el reconocimiento de entidades nombradas en español.


## Autor
Pablo C. Ruiz







