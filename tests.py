import unittest
import requests

class TestAPI(unittest.TestCase):

    def test_ner_api_single_sentence(self):
        # Prueba con una sola oración en el JSON
        url = 'http://localhost:4000/ner'
        data = {
            "oraciones": [
                "La capital de Venezuela es Caracas."
            ]
        }
        response = requests.post(url, json=data)

        self.assertEqual(response.status_code, 200)

        resultado_esperado = {
            "resultado": [
                {
                    "entidades": {
                        "Venezuela": "LOC",
                        "Caracas": "LOC"
                    },
                    "oración": "La capital de Venezuela es Caracas."
                }
            ]
        }
        self.assertEqual(response.json(), resultado_esperado)
        print(response.json())



    def test_ner_api_empty_sentences(self):
        # Prueba con un JSON que contiene una lista vacía de oraciones
        url = 'http://localhost:4000/ner'
        data = {
            "oraciones": []
        }
        response = requests.post(url, json=data)

        self.assertEqual(response.status_code, 200)

        resultado_esperado = {
            "resultado": []
        }
        self.assertEqual(response.json(), resultado_esperado)
        print(response.json())


    def test_ner_api_no_entities(self):
        # Prueba con un JSON que contiene oraciones sin entidades nombradas
        url = 'http://localhost:4000/ner'
        data = {
            "oraciones": [
                "El cielo está despejado.",
                "Hoy es un buen día."
            ]
        }
        response = requests.post(url, json=data)

        self.assertEqual(response.status_code, 200)

        resultado_esperado = {
            "resultado": [
                {
                    "entidades": {},
                    "oración": "El cielo está despejado."
                },
                {
                    "entidades": {},
                    "oración": "Hoy es un buen día."
                }
            ]
        }
        self.assertEqual(response.json(), resultado_esperado)
        print(response.json())

    def test_ner_api_two_entities(self):
        # Prueba con una oración que contiene dos entidades
        url = 'http://localhost:4000/ner'
        data = {
            "oraciones": [
                "El Taj Mahal es un famoso mausoleo en la India construido en mármol blanco hace muchos años."
            ]
        }
        response = requests.post(url, json=data)

        self.assertEqual(response.status_code, 200)

        resultado_esperado = {
            "resultado": [
                {
                    "entidades": {
                        "Taj Mahal": "LOC",
                        "la India": "LOC"
                    },
                    "oración": "El Taj Mahal es un famoso mausoleo en la India construido en mármol blanco hace muchos años."
                }
            ]
        }
        self.assertEqual(response.json(), resultado_esperado)
        print(response.json())

    def test_ner_api_special_characters(self):
        # Prueba con una oración que contiene caracteres especiales
        # Esta prueba falla porque el NLP de spacy no está listo para este tipo de prueba
        # Haría falta buscar otro modelo o entrenar uno propio para manejo de carateres especiales como signos de exclamación
        url = 'http://localhost:4000/ner'
        data = {
            "oraciones": [
                "¡Hola! ¿Cómo estás? La dirección es Calle 123, #456."
            ]
        }
        response = requests.post(url, json=data)

        self.assertEqual(response.status_code, 200)

        resultado_esperado = {
            "resultado": [
                {
                    "oración": "¡Hola! ¿Cómo estás? La dirección es Calle 123, #456.",
                    "entidades": {
                        "Calle 123, #456": "LOC"
                    }
                }
            ]
        }
        self.assertNotEqual(response.json(), resultado_esperado)
        print(response.json())

    def test_ner_api_invalid_json(self):
        # Prueba con un JSON inválido, en este caso un string común, el handle del error se hace directamente en la app
        url = 'http://localhost:4000/ner'
        data = "Esto no es un JSON válido"
        response = requests.post(url, json=data)

        self.assertEqual(response.status_code, 500)
        print(response.json())

    def test_ner_api_empty_sentence(self):
        # Prueba con una oración vacía
        url = 'http://localhost:4000/ner'
        data = {
            "oraciones": [
                ""
            ]
        }
        response = requests.post(url, json=data)

        self.assertEqual(response.status_code, 200)

        resultado_esperado = {
            "resultado": [
                {
                    "oración": "",
                    "entidades": {}
                }
            ]
        }
        self.assertEqual(response.json(), resultado_esperado)
        print(response.json())

    def test_ner_api_large_request(self):
        # Prueba con muchas oraciones (tomadas de https://www.ejemplos.co/oraciones-simples/)
        # interesante como el modelo toma la palabra "Tengo como un miscelaneo"

        url = 'http://localhost:4000/ner'
        data = {
            "oraciones": [
                "Mi abuela cocinó fideos con estofado.",
                "El sol saldrá a las 6.30 de la mañana.",
                "Damián se cortó el pelo.",
                "Mi tía fue al supermercado en el auto.",
                "Me compré una bicicleta nueva.",
                "Tengo turno con el dentista a las 18 horas.",
                "Mañana nos vamos de campamento.",
                "El intendente fue reelecto.",
                "La profesora explicó las causas de la Revolución francesa.",
                "Tengo entradas para el teatro.",
                "La librería cierra los domingos.",
                "Preparé una torta para mi cumpleaños.",
                "Argentina es un país de América del Sur.",
                "Este año comienzo la facultad.",
                "La pareja se casó la semana pasada.",
                "Almorcemos en este restaurante mañana.",
                "¿Te gustó el último disco de la banda?",
                "Le compré un ramo de flores.",
                "El verdulero no tenía cambio.",
                "La pizarra está toda escrita.",
                "Ábreme este frasco.",
                "Ya terminé el libro de Milan Kundera.",
                "El calentamiento global es un problema de todos.",
                "La ventana está muy sucia.",
                "Manuel apagó la computadora.",
                "El mapa de África quedó en la escuela.",
                "Dejé las entradas para el recital en la mesa de la cocina.",
                "La escritora presentó su libro en la feria.",
                "Mirta suspendió su almuerzo.",
                "El grabador se descompuso después del corte de luz.",
                "Todos los lunes entrenamos a la mañana.",
                "Tengo ganas de tomar un helado.",
                "No estoy de acuerdo con la decisión tomada.",
                "Las acciones de la empresa aumentaron este año.",
                "Los clientes de la cafetería del barrio son muy fieles.",
                "Mis plantas nunca dan flores.",
                "Te voy a regalar un libro de cocina.",
                "El acusado y su abogada abandonaron la sala.",
                "La función comenzó a horario.",
                "Te estuve esperando durante toda la mañana.",
                "No conozco a nadie en este lugar.",
                "Volvió para contarle la verdad a su amigo.",
                "En el trabajo nuevo me pagan muy bien.",
                "Tengo grandes proyectos para este año.",
                "¿Vamos a andar en bicicleta?",
                "Estoy aprendiendo a hablar en francés.",
                "Mi hermana y mi tía se fueron de viaje.",
                "Se rompió el teclado de mi computadora.",
                "El niño salió corriendo después de su travesura.",
                "Ya aprendí la lección."
            ]
        }
        response = requests.post(url, json=data)

        self.assertEqual(response.status_code, 200)


        resultado_esperado = {
            "resultado": [
                {
                    "entidades": {},
                    "oración": "Mi abuela cocinó fideos con estofado."
                },
                {
                    "entidades": {},
                    "oración": "El sol saldrá a las 6.30 de la mañana."
                },
                {
                    "entidades": {
                        "Damián": "PER"
                    },
                    "oración": "Damián se cortó el pelo."
                },
                {
                    "entidades": {
                        "Mi tía": "MISC"
                    },
                    "oración": "Mi tía fue al supermercado en el auto."
                },
                {
                    "entidades": {},
                    "oración": "Me compré una bicicleta nueva."
                },
                {
                    "entidades": {
                        "Tengo": "MISC"
                    },
                    "oración": "Tengo turno con el dentista a las 18 horas."
                },
                {
                    "entidades": {
                        "Mañana": "PER"
                    },
                    "oración": "Mañana nos vamos de campamento."
                },
                {
                    "entidades": {},
                    "oración": "El intendente fue reelecto."
                },
                {
                    "entidades": {
                        "Revolución": "MISC"
                    },
                    "oración": "La profesora explicó las causas de la Revolución francesa."
                },
                {
                    "entidades": {
                        "Tengo": "MISC"
                    },
                    "oración": "Tengo entradas para el teatro."
                },
                {
                    "entidades": {},
                    "oración": "La librería cierra los domingos."
                },
                {
                    "entidades": {
                        "Preparé": "PER"
                    },
                    "oración": "Preparé una torta para mi cumpleaños."
                },
                {
                    "entidades": {
                        "América del Sur": "LOC",
                        "Argentina": "LOC"
                    },
                    "oración": "Argentina es un país de América del Sur."
                },
                {
                    "entidades": {},
                    "oración": "Este año comienzo la facultad."
                },
                {
                    "entidades": {},
                    "oración": "La pareja se casó la semana pasada."
                },
                {
                    "entidades": {},
                    "oración": "Almorcemos en este restaurante mañana."
                },
                {
                    "entidades": {},
                    "oración": "¿Te gustó el último disco de la banda?"
                },
                {
                    "entidades": {},
                    "oración": "Le compré un ramo de flores."
                },
                {
                    "entidades": {},
                    "oración": "El verdulero no tenía cambio."
                },
                {
                    "entidades": {},
                    "oración": "La pizarra está toda escrita."
                },
                {
                    "entidades": {},
                    "oración": "Ábreme este frasco."
                },
                {
                    "entidades": {
                        "Milan Kundera": "PER"
                    },
                    "oración": "Ya terminé el libro de Milan Kundera."
                },
                {
                    "entidades": {},
                    "oración": "El calentamiento global es un problema de todos."
                },
                {
                    "entidades": {},
                    "oración": "La ventana está muy sucia."
                },
                {
                    "entidades": {
                        "Manuel": "PER"
                    },
                    "oración": "Manuel apagó la computadora."
                },
                {
                    "entidades": {
                        "África quedó": "PER"
                    },
                    "oración": "El mapa de África quedó en la escuela."
                },
                {
                    "entidades": {
                        "Dejé": "MISC"
                    },
                    "oración": "Dejé las entradas para el recital en la mesa de la cocina."
                },
                {
                    "entidades": {},
                    "oración": "La escritora presentó su libro en la feria."
                },
                {
                    "entidades": {},
                    "oración": "Mirta suspendió su almuerzo."
                },
                {
                    "entidades": {},
                    "oración": "El grabador se descompuso después del corte de luz."
                },
                {
                    "entidades": {},
                    "oración": "Todos los lunes entrenamos a la mañana."
                },
                {
                    "entidades": {
                        "Tengo": "MISC"
                    },
                    "oración": "Tengo ganas de tomar un helado."
                },
                {
                    "entidades": {},
                    "oración": "No estoy de acuerdo con la decisión tomada."
                },
                {
                    "entidades": {
                        "año": "MISC"
                    },
                    "oración": "Las acciones de la empresa aumentaron este año."
                },
                {
                    "entidades": {},
                    "oración": "Los clientes de la cafetería del barrio son muy fieles."
                },
                {
                    "entidades": {},
                    "oración": "Mis plantas nunca dan flores."
                },
                {
                    "entidades": {},
                    "oración": "Te voy a regalar un libro de cocina."
                },
                {
                    "entidades": {},
                    "oración": "El acusado y su abogada abandonaron la sala."
                },
                {
                    "entidades": {},
                    "oración": "La función comenzó a horario."
                },
                {
                    "entidades": {},
                    "oración": "Te estuve esperando durante toda la mañana."
                },
                {
                    "entidades": {},
                    "oración": "No conozco a nadie en este lugar."
                },
                {
                    "entidades": {
                        "Volvió": "LOC"
                    },
                    "oración": "Volvió para contarle la verdad a su amigo."
                },
                {
                    "entidades": {},
                    "oración": "En el trabajo nuevo me pagan muy bien."
                },
                {
                    "entidades": {
                        "Tengo": "MISC",
                        "año": "MISC"
                    },
                    "oración": "Tengo grandes proyectos para este año."
                },
                {
                    "entidades": {},
                    "oración": "¿Vamos a andar en bicicleta?"
                },
                {
                    "entidades": {
                        "Estoy": "PER"
                    },
                    "oración": "Estoy aprendiendo a hablar en francés."
                },
                {
                    "entidades": {},
                    "oración": "Mi hermana y mi tía se fueron de viaje."
                },
                {
                    "entidades": {},
                    "oración": "Se rompió el teclado de mi computadora."
                },
                {
                    "entidades": {},
                    "oración": "El niño salió corriendo después de su travesura."
                },
                {
                    "entidades": {},
                    "oración": "Ya aprendí la lección."
                }
            ]
        }

        self.assertEqual(response.status_code, 200)
        print(response.json())




    def test_ner_api_no_input(self):
        # Prueba con un JSON que no contiene la clave "oraciones"
        url = 'http://localhost:4000/ner'
        data = {}
        response = requests.post(url, json=data)


        self.assertEqual(response.status_code, 400)
        print(response.json())


    def test_ner_api_invalid_input(self):
        # Prueba con un JSON que contiene una clave inválida
        url = 'http://localhost:4000/ner'
        data = {
            "sentences": [
                "Esto es una oración de prueba."
            ]
        }

        response = requests.post(url, json=data)

        self.assertEqual(response.status_code, 400)
        print(response.json())


    def test_ner_api_many_entities(self):
        # Prueba con oraciones que contienen muchas entidades nombradas
        # Muy interesante como el modelo es muy preciso
        url = 'http://localhost:4000/ner'
        data = {
            "oraciones": [
                "Elon Musk es el CEO de Tesla y SpaceX. Además, fundó Neuralink, The Boring Company y OpenAI.",
                "La Mona Lisa de Leonardo da Vinci es una obra famosa del Renacimiento. Se encuentra en el Louvre en París, Francia.",
                "El científico Albert Einstein nació en Ulm, Alemania. Es conocido por su teoría de la relatividad y ganó el premio Nobel de Física en 1921.",
                "La NASA, la Agencia Espacial Europea (ESA) y Roscosmos colaboran en misiones espaciales internacionales.",
                "En la película El Señor de los Anillos: La Comunidad del Anillo, Frodo Bolsón es interpretado por Elijah Wood."
            ]
        }
        response = requests.post(url, json=data)

        self.assertEqual(response.status_code, 200)

        resultado_esperado = {
            "resultado": [
                {
                    "entidades": {
                        "Elon Musk": "LOC",
                        "OpenAI": "MISC",
                        "SpaceX. Además": "PER",
                        "Tesla": "LOC",
                        "The Boring Company": "ORG",
                        "fundó Neuralink": "PER"
                    },
                    "oración": "Elon Musk es el CEO de Tesla y SpaceX. Además, fundó Neuralink, The Boring Company y OpenAI."
                },
                {
                    "entidades": {
                        "Francia": "LOC",
                        "La Mona Lisa": "PER",
                        "Leonardo da Vinci": "PER",
                        "Louvre": "LOC",
                        "París": "LOC",
                        "Renacimiento": "PER"
                    },
                    "oración": "La Mona Lisa de Leonardo da Vinci es una obra famosa del Renacimiento. Se encuentra en el Louvre en París, Francia."
                },
                {
                    "entidades": {
                        "Albert Einstein": "PER",
                        "Alemania": "LOC",
                        "Nobel de Física": "MISC",
                        "Ulm": "LOC"
                    },
                    "oración": "El científico Albert Einstein nació en Ulm, Alemania. Es conocido por su teoría de la relatividad y ganó el premio Nobel de Física en 1921."
                },
                {
                    "entidades": {
                        "Agencia Espacial Europea": "ORG",
                        "ESA": "ORG",
                        "NASA": "ORG",
                        "Roscosmos": "ORG"
                    },
                    "oración": "La NASA, la Agencia Espacial Europea (ESA) y Roscosmos colaboran en misiones espaciales internacionales."
                },
                {
                    "entidades": {
                        "El Señor de los Anillos: La Comunidad del Anillo": "MISC",
                        "Elijah Wood": "PER",
                        "Frodo Bolsón": "PER"
                    },
                    "oración": "En la película El Señor de los Anillos: La Comunidad del Anillo, Frodo Bolsón es interpretado por Elijah Wood."
                }
            ]
        }
        self.assertEqual(response.json(), resultado_esperado)
        print(response.json())



    def test_ner_api(self):
        # Pruena original del reto
        url = 'http://localhost:4000/ner'
        data = {
            "oraciones": [
                "Apple está buscando comprar una startup del Reino Unido por mil millones de dólares.",
                "San Francisco considera prohibir los robots de entrega en la acera."
            ]
        }
        response = requests.post(url, json=data)

        self.assertEqual(response.status_code, 200)

        resultado_esperado = {
            "resultado": [
                {
                    "entidades": {
                        "Apple": "ORG",
                        "Reino Unido": "LOC"
                    },
                    "oración": "Apple está buscando comprar una startup del Reino Unido por mil millones de dólares."
                },
                {
                    "entidades": {
                        "San Francisco": "LOC"
                    },
                    "oración": "San Francisco considera prohibir los robots de entrega en la acera."
                }
            ]
        }
        self.assertEqual(response.json(), resultado_esperado)
        print(response.json())


if __name__ == '__main__':
    unittest.main()
