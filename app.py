
# Hay que instalar flask

# pip3 install flask
from flask import Flask, jsonify, request

import json

#Hay que instalar spacy y el modelo de español solicitado en las instrucciones

#pip3 install spacy
import spacy

# Las instalaciones de spacy y de Flask se pueden hacer directamente ejecutando <pip3 install -r requirements.txt>

app = Flask(__name__)

#hay que descargar el modelo de NLP con <python3 -m spacy download es_core_news_sm>

#y usarlo con 
#nlp = spacy.load('es_core_news_sm')


# o bien, con el siguiente try se puede descargar y montar en caso de que ya este descargado
try:
    nlp = spacy.load('es_core_news_sm')
except OSError:
    spacy.cli.download('es_core_news_sm')
    nlp = spacy.load('es_core_news_sm')


# Solo se requiere una ruta para el post que solicitan las instrucciones
@app.route('/ner', methods=['POST'])
def ner():
    # Se manejarán excepciones para poder evaluar la API
    try:
        data = request.get_json()

        # Hay que validar si se tiene la clave oraciones, requisito de la API segun las instrucciones
        oraciones = data.get('oraciones')

        if not isinstance(oraciones, list):
            raise ValueError("El JSON debe tener una clave 'oraciones' con una lista de oraciones en español.")


        # Procesamiento del JSON enviado con NLP
        resultado = []
        for oracion in oraciones:
            # Se analiza cada oracion con NLP 
            doc = nlp(oracion)
            entidades = {}
            for entidad in doc.ents:
                entidades[entidad.text] = entidad.label_
            
            # Se construye el resultado segun las oraciones que lleguen y las entidades que se encuentren
            resultado.append({
                "oración": oracion,
                "entidades": entidades
            })
        
        # Si no hubo ninguna excepción, se regresa status 200 y el resultado en json
        return jsonify({"resultado": resultado}), 200
    
    # Si no se tiene la clave oraciones, se regresa status 400
    except ValueError as e:
        return jsonify({"error": str(e)}), 400

    # Si hay algun otro error, se regresa status 500
    except Exception as e:
        return jsonify({"error": "Ocurrió un error en el servidor. Detalles: " + str(e)}), 500



if __name__ == "__main__":
    # Se ejecuta la API en localhost con puerto 4000
    app.run(debug = True, port = 4000)

